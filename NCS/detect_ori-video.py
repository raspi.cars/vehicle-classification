# Import packages
import os
import cv2 
import sys

# This is needed since the notebook is stored in the object_detection folder.
sys.path.append("..")

# Name of the directory containing the object detection module we're using
VIDEO_NAME = 'Pideo15.h264'
VIDEO_PATH = '/home/pi/car_class'

# Grab path to current working directory
CWD_PATH = '/home/pi/car-class/training'

# Path to frozen detection graph .pb file, which contains the model that is used
# for object detection.
# Path to video
PATH_TO_VIDEO = os.path.join(VIDEO_PATH,VIDEO_NAME)

# Number of classes the object detector can identify

# Load the label map.
# Label maps map indices to category names, so that when our convolution
# network predicts `5`, we know that this corresponds to `king`.
# Here we use internal utility functions, but anything that returns a
# dictionary mapping integers to appropriate string labels would be fine
net = cv2.dnn_DetectionModel('../training/SSD_1.14.xml',
                            '../training/SSD_1.14.bin')
# Specify target device.
net.setPreferableTarget(cv2.dnn.DNN_TARGET_MYRIAD)

# Open video file
video = cv2.VideoCapture(PATH_TO_VIDEO)

if (video.isOpened()== False): 
  print("Error opening video stream or file")

while(video.isOpened()):

    # Acquire frame and expand frame dimensions to have shape: [1, None, None, 3]
    # i.e. a single-column array, where each item in the column has the pixel RGB value
    ret, frame = video.read()
    _, confidences, boxes = net.detect(frame, confThreshold=0.5)

    # All the results have been drawn on the frame, so it's time to display it.
    
    for confidence, box in zip(list(confidences), boxes):
        cv2.rectangle(frame, box, color=(0, 255, 0))
    cv2.imshow('Object detector', frame)
    # Press 'q' to quit
    if cv2.waitKey(1) == ord('q'):
        break

# Clean up
video.release()
cv2.destroyAllWindows()
