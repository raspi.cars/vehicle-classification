######## Video Object Detection Using Tensorflow-trained Classifier #########
#
# Author: Evan Juras
# Date: 1/16/18
# Description: 
# This program uses a TensorFlow-trained classifier to perform object detection.
# It loads the classifier and uses it to perform object detection on a video.
# It draws boxes, scores, and labels around the objects of interest in each
# frame of the video.

## Some of the code is copied from Google's example at
## https://github.com/tensorflow/models/blob/master/research/object_detection/object_detection_tutorial.ipynb

## and some is copied from Dat Tran's example at
## https://github.com/datitran/object_detector_app/blob/master/object_detection_app.py

## but I changed it to make it more understandable to me.

# Import packages
import os
import cv2 
import sys

# This is needed since the notebook is stored in the object_detection folder.
sys.path.append("..")



# Name of the directory containing the object detection module we're using
VIDEO_NAME = 'Pideo15.h264'
VIDEO_PATH = '/home/pi/car_class'

# Grab path to current working directory
CWD_PATH = '/home/pi/car-class/training'

# Path to frozen detection graph .pb file, which contains the model that is used
# for object detection.
PATH_TO_CKPT = os.path.join(CWD_PATH,'ssd_mobilenet_v2_step3000.pb')

# Path to label map file
PATH_TO_LABELS = os.path.join(CWD_PATH,'label_map.pbtxt')

# Path to video
PATH_TO_VIDEO = os.path.join(VIDEO_PATH,VIDEO_NAME)

# Number of classes the object detector can identify
NUM_CLASSES = 3

# Load the label map.
# Label maps map indices to category names, so that when our convolution
# network predicts `5`, we know that this corresponds to `king`.
# Here we use internal utility functions, but anything that returns a
# dictionary mapping integers to appropriate string labels would be fine
net = cv2.dnn_DetectionModel('SSDv1.xml',
                            'SSDv1.bin')
# Specify target device.
net.setPreferableTarget(cv2.dnn.DNN_TARGET_MYRIAD)

# Open video file
video = cv2.VideoCapture(PATH_TO_VIDEO)

if (video.isOpened()== False): 
  print("Error opening video stream or file")

while(video.isOpened()):

    # Acquire frame and expand frame dimensions to have shape: [1, None, None, 3]
    # i.e. a single-column array, where each item in the column has the pixel RGB value
    ret, frame = video.read()
    cek, confidences, boxes = net.detect(frame, confThreshold=0.5)

    # All the results have been drawn on the frame, so it's time to display it.
    
    for confidence, box in zip(list(confidences), boxes):
        cv2.rectangle(frame, box, color=(0, 255, 0))
    cv2.imshow('Object detector', frame)
    # Press 'q' to quit
    if cv2.waitKey(1) == ord('q'):
        break

# Clean up
video.release()
cv2.destroyAllWindows()
