import sys
import os
import numpy as np
import cv2
from os import system
import io, time
from os.path import isfile, join
import re

fps = ""
detectfps = ""
framecount = 0
detectframecount = 0
time1 = 0
time2 = 0

LABELS = ('motorbike', 'car', 'truck')

VIDEO_NAME = 'Pideo15.h264'
VIDEO_PATH = '/home/pi/car_class'

PATH_TO_VIDEO = os.path.join(VIDEO_PATH,VIDEO_NAME)

cap = cv2.VideoCapture(PATH_TO_VIDEO)

# net = cv2.dnn.readNet('/home/pi/car_class/training/SSD_1.14_50k_old+truck+v2+v3.xml', '/home/pi/car_class/training/SSD_1.14_50k_old+truck+v2+v3.bin')
net = cv2.dnn.readNet('/home/pi/car_class/training/SSD_1.14_50k_old+truck+v2+v3.xml', '/home/pi/car_class/training/SSD_1.14_50k_old+truck+v2+v3.bin')
net.setPreferableTarget(cv2.dnn.DNN_TARGET_MYRIAD)

if (cap.isOpened()== False):
    print("Error opening video stream or file")

while(cap.isOpened()):

    t1 = time.perf_counter()

    ret, color_image = cap.read()
    if not ret:
        break

    height = color_image.shape[0]
    width = color_image.shape[1]

    blob = cv2.dnn.blobFromImage(color_image, 1, swapRB=True, crop=False)
    net.setInput(blob)
    out = net.forward()
    out = out.flatten()

    for box_index in range(100):
        print("aaaaa")
        if out[box_index + 1] == 0.0:
            print("bbbbbb")
            break
        base_index = box_index * 7
        if (not np.isfinite(out[base_index]) or
            not np.isfinite(out[base_index + 1]) or
            not np.isfinite(out[base_index + 2]) or
            not np.isfinite(out[base_index + 3]) or
            not np.isfinite(out[base_index + 4]) or
            not np.isfinite(out[base_index + 5]) or
            not np.isfinite(out[base_index + 6])):
            print("cccccc")
            continue

        if box_index == 0:
            detectframecount += 1

        x1 = int(out[base_index + 3] * height)
        y1 = int(out[base_index + 4] * width)
        x2 = int(out[base_index + 5] * height)
        y2 = int(out[base_index + 6] * width)
        
        
        # ~ for i in range(6):
            # ~ print("out ", i, " = ", out[base_index + i])
        
        print("x1 = ", x1)
        print("y1 = ", y1)
        print("x2 = ", x2)
        print("y2 = ", y2)

        object_info_overlay = out[base_index:base_index + 7]

        min_score_percent = 10
        source_image_width = width
        source_image_height = height

        base_index = 0
        class_id = object_info_overlay[base_index + 1]
        percentage = int(object_info_overlay[base_index + 2] * 100)
        if (percentage <= min_score_percent):
            continue

        box_left = int(object_info_overlay[base_index + 3] * source_image_width)
        box_top = int(object_info_overlay[base_index + 4] * source_image_height)
        box_right = int(object_info_overlay[base_index + 5] * source_image_width)
        box_bottom = int(object_info_overlay[base_index + 6] * source_image_height)
        label_text = LABELS[int(class_id)] + " (" + str(percentage) + "%)"

        box_color = (255, 128, 0)
        box_thickness = 2
        cv2.rectangle(color_image, (box_left, box_top), (box_right, box_bottom), box_color, box_thickness)

        label_background_color = (125, 175, 75)
        label_text_color = (255, 255, 255)

        label_size = cv2.getTextSize(label_text, cv2.FONT_HERSHEY_SIMPLEX, 0.5, 1)[0]
        label_left = box_left
        label_top = box_top - label_size[1]
        if (label_top < 1):
            label_top = 1
        label_right = label_left + label_size[0]
        label_bottom = label_top + label_size[1]
        cv2.rectangle(color_image, (label_left - 1, label_top - 1), (label_right + 1, label_bottom + 1), label_background_color, -1)
        cv2.putText(color_image, label_text, (label_left, label_bottom), cv2.FONT_HERSHEY_SIMPLEX, 0.5, label_text_color, 1)

    cv2.putText(color_image, fps,       (width-170,15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (38,0,255), 1, cv2.LINE_AA)
    cv2.putText(color_image, detectfps, (width-170,30), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (38,0,255), 1, cv2.LINE_AA)

    cv2.namedWindow('detect video', cv2.WINDOW_AUTOSIZE)
    cv2.imshow('detect video', cv2.resize(color_image, (width, height)))

    if cv2.waitKey(1)&0xFF == ord('q'):
        break

    # FPS calculation
    framecount += 1
    if framecount >= 15:
        fps       = "(Playback) {:.1f} FPS".format(time1/15)
        detectfps = "(Detection) {:.1f} FPS".format(detectframecount/time2)
        framecount = 0
        detectframecount = 0
        time1 = 0
        time2 = 0
    t2 = time.perf_counter()
    elapsedTime = t2-t1
    time1 += 1/elapsedTime
    time2 += elapsedTime
