# -*- coding: utf-8 -*-
"""
Created on Wed Sep 13 13:59:59 2017

@author: philanderz
"""

from random import randint
import time

class MyVehicle:
    tracks = []
    def __init__(self, i, xi, yi, type, max_age):
        self.i = i
        self.x = xi
        self.y = yi
        self.type = type
        self.tracks = []
        self.R = randint(0,255)
        self.G = randint(0,255)
        self.B = randint(0,255)
        self.done = False
        self.state = '0'
        self.age = 0
        self.max_age = max_age
        self.dir = None
        self.crossed_start = False
        self.crossed_end = False
    def getRGB(self):
        return (self.R,self.G,self.B)
    def getTracks(self):
        return self.tracks
    def getId(self):
        return self.i
    def getState(self):
        return self.state
    def getDir(self):
        return self.dir
    def getX(self):
        return self.x
    def getY(self):
        return self.y
    def getType(self):
        return self.type
    def updateCoords(self, xn, yn):
        self.age = 0
        self.tracks.append([self.x,self.y])
        self.x = xn
        self.y = yn
    def setDone(self):
        self.done = True
    def timedOut(self):
        return self.done
    def going_UP(self,mid_start,mid_end):
        if len(self.tracks) >= 2:
            if self.state == '0':
                if self.tracks[-1][1] < mid_end and self.tracks[-2][1] >= mid_end: #cruzo la linea
                    self.state = '1'
                    self.dir = 'up'
                    return True
            else:
                return False
        else:
            return False
    def going_DOWN(self,start,end):
        if len(self.tracks) >= 2:
            if self.state == '0':
                if self.tracks[-1][1] > start and self.tracks[-2][1] <= start: #cruzo la linea
                    # self.crossed_start = True
                # if self.tracks[-1][1] > end and self.tracks[-2][1] < end:
                #     self.crossed_end = True
                # if self.crossed_start and self.crossed_end:
                    self.state = '1'
                    self.dir = 'down'
                    # self.crossed_start = False
                    # self.crossed_end = False
                    return True
            else:
                return False
        else:
            return False
    def age_one(self):
        self.age += 1
        if self.age > self.max_age:
            self.done = True
        return True
# class MultiPerson:
#     def __init__(self, persons, xi, yi):
#         self.vehicles = vehicles
#         self.x = xi
#         self.y = yi
#         self.tracks = []
#         self.R = randint(0,255)
#         self.G = randint(0,255)
#         self.B = randint(0,255)
#         self.done = False
