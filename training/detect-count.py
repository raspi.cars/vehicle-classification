# Import packages
import os
import cv2
import numpy as np
import tensorflow as tf
import sys
import time
import Vehicle2
# ~ from picamera.array import PiRGBArray
# ~ from picamera import PiCamera

# Import utilites
from utils import label_map_util
from utils import visualization_utils as vis_util

# ~ sys.path.append("..")
# ~ sys.path.append("C:\\Users\\Aljovan Magyarsa P\\Google Drive\\Documents\\Data Kuliah\\Kerja Praktek\\Tensorflow\\models\\research\\slim")
# ~ sys.path.append("C:/Users/Aljovan Magyarsa P/Google Drive/Documents/Data Kuliah/Kerja Praktek/Tensorflow/models/research/object_detection")
# ~ sys.path.append("C:\\Users\\Aljovan Magyarsa P\\Google Drive\\Documents\\Data Kuliah\\Kerja Praktek\\Tensorflow\\models\\research")

centers=[]
cnt_down = 0
DownLV = 0
DownHV = 0
DownMTR = 0

# Name of the directory containing the object detection module we're using
VIDEO_NAME = 'Pideo13.h264'
VIDEO_PATH = '/home/rara/vehicle-classification'
# Grab path to current working directory
CWD_PATH = os.getcwd()

# Path to frozen detection graph .pb file, which contains the model that is used
# for object detection.
PATH_TO_CKPT = os.path.join(CWD_PATH,'SSD_1.14_200k.pb')

# Path to label map file
PATH_TO_LABELS = os.path.join(CWD_PATH,'label_map.pbtxt')

# Path to video
PATH_TO_VIDEO = os.path.join(VIDEO_PATH,VIDEO_NAME)

# Number of classes the object detector can identify
NUM_CLASSES = 3

# Load the label map.
# Label maps map indices to category names, so that when our convolution
# network predicts `5`, we know that this corresponds to `king`.
# Here we use internal utility functions, but anything that returns a
# dictionary mapping integers to appropriate string labels would be fine
label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)

# Load the Tensorflow model into memory.
detection_graph = tf.Graph()
with detection_graph.as_default():
    od_graph_def = tf.GraphDef()
    with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')

    sess = tf.Session(graph=detection_graph)

# Define input and output tensors (i.e. data) for the object detection classifier

# Input tensor is the image
image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')

# Output tensors are the detection boxes, scores, and classes
# Each box represents a part of the image where a particular object was detected
detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')

# Each score represents level of confidence for each of the objects.
# The score is shown on the result image, together with the class label.
detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')

# Number of objects detected
num_detections = detection_graph.get_tensor_by_name('num_detections:0')

# Open video file
video = cv2.VideoCapture(PATH_TO_VIDEO)
(major_ver, minor_ver, subminor_ver) = (cv2.__version__).split('.')
     
# ~ camera = PiCamera()
# ~ camera.resolution = (256, 192)
# ~ camera.framerate = 60
# ~ rawCapture = PiRGBArray(camera, size=(256, 192))
# ~ time.sleep(0.1)

pid = 1

while(video.isOpened()):
# ~ for framee in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
    # ~ im2 = framee.array
    # ~ im = im2.copy()
    # ~ im.setflags(write=1)
    
    # Acquire frame and expand frame dimensions to have shape: [1, None, None, 3]
    # i.e. a single-column array, where each item in the column has the pixel RGB value
    ret, raw= video.read()
    scale_percent = 40 # percent of original size
    width = int(raw.shape[1] * scale_percent / 100)
    height = int(raw.shape[0] * scale_percent / 100)
    dim = (width, height)
    im = cv2.resize(raw, dim, interpolation = cv2.INTER_AREA)
    # ~ frame = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
    frame = im
    frame_expanded = np.expand_dims(frame, axis=0)
    h = height
    w = width
    
    # Input/Output Lines
    # setting posisi garis
    line_down = int(7*(h/10))
    down_limit = int(8*(h/10))
    line_down_color = (255,0,0)
    # set height garis
    pt1 = [0, line_down]
    # set lebar garis sesuai width video
    pt2 = [w, line_down]
    pts_L1 = np.array([pt1,pt2], np.int32)
    pts_L1 = pts_L1.reshape((-1,1,2))
    
    pt7 = [0, down_limit]
    pt8 = [w, down_limit]
    pts_L4 = np.array([pt7,pt8], np.int32)
    pts_L4 = pts_L4.reshape((-1,1,2))

    font = cv2.FONT_HERSHEY_SIMPLEX
    vehicles = []
    max_age = 5

    # for i in vehicles:
    #     i.age_one()  # age every person on frame

    # Perform the actual detection by running the model with the image as input
    (boxes, scores, classes, num) = sess.run(
        [detection_boxes, detection_scores, detection_classes, num_detections],
        feed_dict={image_tensor: frame_expanded})
    

    min_thres = 0.4
    # Draw the results of the detection (aka 'visulaize the results')    
    vis_util.visualize_boxes_and_labels_on_image_array(
    frame,
    np.squeeze(boxes),
    np.squeeze(classes).astype(np.int32),
    np.squeeze(scores),
    category_index,
    use_normalized_coordinates=True,
    line_thickness=1,
    min_score_thresh=min_thres)

    coordinates = vis_util.return_coordinates(
                        frame,
                        np.squeeze(boxes),
                        np.squeeze(classes).astype(np.int32),
                        np.squeeze(scores),
                        category_index,
                        use_normalized_coordinates=True,
                        line_thickness=8,
                        min_score_thresh=min_thres)
    
    coor_len = len(coordinates)
    # print("coordinates = ", coordinates )
    for j in range(0,coor_len):
        ymin = coordinates[j][0]
        ymax = coordinates[j][1]
        xmin = coordinates[j][2]
        xmax = coordinates[j][3]
        classname = coordinates[j][4]
        cy = int((ymin + ymax)/2)
        cx = int((xmin + xmax)/2)
        # c = cy, cx
        # centers.append(c)
        # # ~ print("c = ",c)
        #
        # print("centers = ", centers)
        # print(len(centers))

        wbox = xmax-xmin
        hbox = ymax-ymin

        new = True

        x = xmin
        y = ymin
        for i in vehicles:
            if abs(x-i.getX()) <= w and abs(y-i.getY()) <= h:
                print("AAAAAAAAAAA")
                new = False
                i.updateCoords(cx,cy)   # Update the coordinates in the object and reset age
                # jika objek melewati garis merah duluan berarti going down, dicek di Vehicle.py
                if i.going_DOWN(line_down, down_limit) == True:
                    roi = frame[y:y+h, x:x+w]
                    print("cx, cy = ", cx, cy)
                    print("class = ", classname)

                    if classname == 'motorbike':
                        DownMTR += 1
                    elif classname == 'car':
                        DownLV += 1
                    elif classname == 'truck':
                        DownHV += 1

                    cnt_down += 1
                    print("Class:", i.getType(), " ID: ", i.getId(), ' crossed going down at', time.strftime("%c"))
                    cv2.putText(frame, str(i.getId()), (cx-5, cy), font, 0.4, (0, 0,255), 1, cv2.LINE_AA)
                break
            if i.getState() == '1':
                print("BBBBBBBBBB")
                if i.getDir() == 'down' and i.getY() > down_limit:
                    i.setDone()
            if i.timedOut():
                print("CCCCCCCCCC")
                # Remove from the list person
                index = vehicles.index(i)
                vehicles.pop(index)
                del i
        if new == True:
            print("DDDDDDDDDD")
            p = Vehicle2.MyVehicle(pid,cx,cy,classname, max_age)
            vehicles.append(p)
            pid += 1

        cv2.circle(frame, (cx, cy), 2, (255, 0, 255), -1)


    # str_down = 'DOWN: ' + str(cnt_down)
    MTR_down = 'Down Motor: ' + str(DownMTR)
    LV_down = 'Down Mobil: ' + str(DownLV)
    HV_down = 'Down Truck/Bus: ' + str(DownHV)
    frame = cv2.polylines(frame, [pts_L1], False, line_down_color, thickness=1)
    frame = cv2.polylines(frame, [pts_L4], False, (255,255,255), thickness=1)

    cv2.putText(frame, MTR_down, (10, 20), font, 0.3, (255, 255, 255), 2, cv2.LINE_AA)
    cv2.putText(frame, MTR_down, (10, 20), font, 0.3, (255, 0, 0), 1, cv2.LINE_AA)
    cv2.putText(frame, LV_down, (10, 30), font, 0.3, (255, 255, 255), 2, cv2.LINE_AA)
    cv2.putText(frame, LV_down, (10, 30), font, 0.3, (255, 0, 0), 1, cv2.LINE_AA)
    cv2.putText(frame, HV_down, (10, 40), font, 0.3, (255, 255, 255), 2, cv2.LINE_AA)
    cv2.putText(frame, HV_down, (10, 40), font, 0.3, (255, 0, 0), 1, cv2.LINE_AA)

    # All the results have been drawn on the frame, so it's time to display it.

    scale_percent = 220 # percent of original size
    width = int(frame.shape[1] * scale_percent / 100)
    height = int(frame.shape[0] * scale_percent / 100)
    dim = (width, height)
    # resize image
    resized = cv2.resize(frame, dim, interpolation = cv2.INTER_AREA)
    cv2.imshow('Object detector', resized)
    if int(major_ver)  < 3 :
        fps = video.get(cv2.cv.CV_CAP_PROP_FPS)
        print("Frames per second using video.get(cv2.cv.CV_CAP_PROP_FPS): {0}".format(fps))
    else :
        fps = video.get(cv2.CAP_PROP_FPS)
        print("Frames per second using video.get(cv2.CAP_PROP_FPS) : {0}".format(fps))
     
	# clear the stream in preparation for the next frame
    # rawCapture.truncate(0)

    # Press 'q' to quit
    if cv2.waitKey(1) == ord('q'):
        break

# Clean up
video.release()
cv2.destroyAllWindows()
