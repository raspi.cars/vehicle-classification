

Untuk user windows download dulu aplikasi git
Untuk user linux sudah ada bawaan di terminal gitnya

Langkah inisiasi git:
1. Jika pertama kalinya menggunakan git:
   a. git config --global user.name "username gitlab"
   b. git config --global user.email alamat email
   c. masuk ke folder ingin ditaruh filenya trus klik kanan pilih git bash here
   d. git clone alamat repo folder git
   e. kalo udah git clone ntar untuk sinkronisasi pake push dan pull aja
2. Biasanya bakal dimintain terus tiap ngepush data ke git username ama pass akun gitlabmu. Kalo mau ga dimintain 
   lagi diatur dulu ssh laptopmu trus diinput di akun gitlabmu. Cari di internat aja ya gimana caranya

Langkah push data:
1. git pull origin master 	#ini supaya data disinkron dulu, kalo ga diginiin ntar di gitlabnya ga sinkron
2. git add . atau git add namafile.formatnya
3. git commit -m "Tulis Pesan commit anda"
4. git push origin master 	#master itu branchnya dituker sesuai kebutuhan yak

Instruksi download versi terakhir:
git pull origin master

Hati hati kalau mengedit satu file yang sama karna bisa jadi file yg sudah diedit jadi hilang. Solusinya
bisa pake branch yang beda. Cari aja di internet maksudnya branch itu apa yak




