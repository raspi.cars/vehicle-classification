# -*- coding: utf-8 -*-
"""
Created on Mon Feb  3 13:19:46 2020

@author: Aljovan Magyarsa P
"""

import numpy as np
import cv2

cap = cv2.VideoCapture('Pideo8.h264')

ret, frame = cap.read()
print('ret =', ret, 'W =', frame.shape[1], 'H =', frame.shape[0], 'channel =', frame.shape[2])


FPS= 40.0
FrameSize=(frame.shape[1], frame.shape[0])
fourcc = cv2.VideoWriter_fourcc(*'MJPG')

out = cv2.VideoWriter('Video_output.avi', fourcc, FPS, FrameSize, 0)
scale_percent = 40 # percent of original size
while(cap.isOpened()):
    ret, frame = cap.read()
    # width = int(frame.shape[1] * scale_percent / 100)
    # height = int(frame.shape[0] * scale_percent / 100)
    # dim = (width, height)
    # im = cv2.resize(frame, dim, interpolation = cv2.INTER_AREA)

    # check for successfulness of cap.read()
    if not ret: break

    # gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    frame = gray
    
    

    # Save the video
    out.write(frame)

    cv2.imshow('frame',frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
         break

cap.release()
out.release()
cv2.destroyAllWindows()