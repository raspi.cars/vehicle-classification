# -*- coding: utf-8 -*-

import cv2
print(cv2.__version__)

cascade_src = 'cascade.xml'
video_src = 'Pideo4.mp4'
#video_src = 'dataset/video2.avi'

cap = cv2.VideoCapture(video_src)
car_cascade = cv2.CascadeClassifier(cascade_src)
i = 1
while True:
    ret, img = cap.read()
    if (type(img) == type(None)):
        break
    
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    
    cars = car_cascade.detectMultiScale(gray, 1.1, 1)

    for (x,y,w,h) in cars:
        cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,255),2)
        cropped = gray[y:y+h, x:x+w]
        cropped2 = img[y:y+h, x:x+w]
        cv2.namedWindow('crop',cv2.WINDOW_NORMAL)
        cv2.resizeWindow('crop', 500,300)
        cv2.namedWindow('Rect',cv2.WINDOW_NORMAL)
        cv2.resizeWindow('Rect', 500,300)
        cv2.imshow('crop', cropped)
        cv2.imshow('Rect', cropped2)
#        if cv2.waitKey(25) == 27:
#            break
#        status = input('status = ')
#        if status == 'y':
#            filename = 'Newcrop' + str(i) + '.jpg'
#            cv2.imwrite(filename, cropped)
#            i = i+1
#            quit = 0
##            a.write(filename + '\t'+ str(x) + '\t' + str(y) + '\t' + str(x+w) +'\t' + str(y+h))
#        if status == 'q':
#            quit = 1
#        else:
#            quit = 0
    
    cv2.imshow('video', img)
    
    if cv2.waitKey(33) == 27:
        break
#        if quit == 1:
#            break
#    if quit == 1:
#        break
cv2.destroyAllWindows()