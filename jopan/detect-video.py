######## Video Object Detection Using Tensorflow-trained Classifier #########
#
# Author: Evan Juras
# Date: 1/16/18
# Description: 
# This program uses a TensorFlow-trained classifier to perform object detection.
# It loads the classifier and uses it to perform object detection on a video.
# It draws boxes, scores, and labels around the objects of interest in each
# frame of the video.

## Some of the code is copied from Google's example at
## https://github.com/tensorflow/models/blob/master/research/object_detection/object_detection_tutorial.ipynb

## and some is copied from Dat Tran's example at
## https://github.com/datitran/object_detector_app/blob/master/object_detection_app.py

## but I changed it to make it more understandable to me.

# Import packages
import os
import cv2
import numpy as np
import tensorflow as tf
import sys

# This is needed since the notebook is stored in the object_detection folder.
# sys.path.append("..")
sys.path.append("E:\\Drive\\Documents\\Data Kuliah\\Kerja Praktek\\Tensorflow\\models\\research\\slim\\")
sys.path.append("E:\\Drive\\Documents\\Data Kuliah\\Kerja Praktek\\Tensorflow\\models\\research\\object_detection\\")
sys.path.append("E:\\Drive\\Documents\\Data Kuliah\\Kerja Praktek\\Tensorflow\\models\\research\\")
# sys.path.append("C:/Program Files/Google Protobuf/bin/")

# Import utilites
from utils import label_map_util
# from object_detection.utils import label_map_util
from utils import visualization_utils as vis_util


# Name of the directory containing the object detection module we're using
VIDEO_NAME = 'Pideo13.h264'
VIDEO_PATH = 'E:/Drive/Documents/Data Kuliah/Kerja Praktek/vehicle edit/Git Raspi/Newgit'

# Grab path to current working directory
CWD_PATH = os.getcwd()
CKPT_PATH = 'E:/Drive/Documents/Data Kuliah/Kerja Praktek/vehicle edit/Git Raspi/Newgit/training'

# Path to frozen detection graph .pb file, which contains the model that is used
# for object detection.
# PATH_TO_CKPT = os.path.join(CWD_PATH,'ssd_mobilenet_v2_truck.pb')
PATH_TO_CKPT = os.path.join(CKPT_PATH,'SSD_1.14_200k.pb')
# PATH_TO_CKPT = os.path.join(CWD_PATH,'frozen_inference_graph_faster_rcnn_inception_v2.pb')

# Path to label map file
PATH_TO_LABELS = os.path.join(CWD_PATH,'label_map.pbtxt')

# Path to video
PATH_TO_VIDEO = os.path.join(VIDEO_PATH,VIDEO_NAME)

# Number of classes the object detector can identify
NUM_CLASSES = 3

# Load the label map.
# Label maps map indices to category names, so that when our convolution
# network predicts `5`, we know that this corresponds to `king`.
# Here we use internal utility functions, but anything that returns a
# dictionary mapping integers to appropriate string labels would be fine
label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)

# Load the Tensorflow model into memory.
detection_graph = tf.Graph()
with detection_graph.as_default():
    od_graph_def = tf.GraphDef()
    with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')

    sess = tf.Session(graph=detection_graph)

# Define input and output tensors (i.e. data) for the object detection classifier

# Input tensor is the image
image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')

# Output tensors are the detection boxes, scores, and classes
# Each box represents a part of the image where a particular object was detected
detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')

# Each score represents level of confidence for each of the objects.
# The score is shown on the result image, together with the class label.
detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')

# Number of objects detected
num_detections = detection_graph.get_tensor_by_name('num_detections:0')

# Open video file
video = cv2.VideoCapture(PATH_TO_VIDEO)

while(video.isOpened()):

    # Acquire frame and expand frame dimensions to have shape: [1, None, None, 3]
    # i.e. a single-column array, where each item in the column has the pixel RGB value
    ret, img = video.read()
    scale_percent = 40 # percent of original size
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    dim = (width, height)
    # resize image
    resizedd = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
    frame = resizedd
    # frame =img
    frame_expanded = np.expand_dims(frame, axis=0)

    # Perform the actual detection by running the model with the image as input
    (boxes, scores, classes, num) = sess.run(
        [detection_boxes, detection_scores, detection_classes, num_detections],
        feed_dict={image_tensor: frame_expanded})

    # print(box_to_color_map.items())q

    # Draw the results of the detection (aka 'visulaize the results')
    vis_util.visualize_boxes_and_labels_on_image_array(
        frame,
        np.squeeze(boxes),
        np.squeeze(classes).astype(np.int32),
        np.squeeze(scores),
        category_index,
        use_normalized_coordinates=True,
        line_thickness=1,
        min_score_thresh=0.4)

    # All the results have been drawn on the frame, so it's time to display it.
    big = frame
    scale_percent = 200 # percent of original size
    width = int(big.shape[1] * scale_percent / 100)
    height = int(big.shape[0] * scale_percent / 100)
    dim = (width, height)
    # resize image
    resized = cv2.resize(big, dim, interpolation = cv2.INTER_AREA)
    # cv2.imshow('Object detector', frame)
    cv2.imshow('Object detector', resized)

    # Press 'q' to quit
    if cv2.waitKey(1) == ord('q'):
        break

# Clean up
video.release()
cv2.destroyAllWindows()
