# -*- coding: utf-8 -*-
"""
Created on Tue Feb 11 11:28:06 2020

@author: Aljovan Magyarsa P
"""

import numpy as np
import cv2
import os

cap = cv2.VideoCapture('E:/Drive/Documents/Data Kuliah/Kerja Praktek/vehicle edit/Git Raspi/Newgit/Pideo14.h264')
i = 0
j = 1
path = 'E:/Drive/Documents/Data Kuliah/Kerja Praktek/vehicle edit/Git Raspi/Newgit/images/raw_original'

while(cap.isOpened()):
	ret, frame = cap.read()
	if ret == False:
		break
	i+=1
	if(i % 30 == 0):
         cv2.imshow('frames', frame)
         img = frame
         cv2.waitKey(10)
         scale_percent = 40 # percent of original size
         width = int(img.shape[1] * scale_percent / 100)
         height = int(img.shape[0] * scale_percent / 100)
         dim = (width, height)
        # resize image
         resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
         name = 'Pideo14-'+ str(j) +'.jpg'
         cv2.imwrite(os.path.join(path ,name), img)
         j+=1
cap.release()
cv2.destroyAllWindows() 