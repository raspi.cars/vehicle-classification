# -*- coding: utf-8 -*-
"""
Created on Mon Jan 13 16:43:35 2020

@author: Aljovan Magyarsa P
"""

# importing csv module 
import csv 
import cv2
import time
import os
  
# csv file name 
filename = "train_labels_truckv3.csv"
  
# initializing the titles and rows list 
fields = [] 
rows = [] 
  
# reading csv file 
with open(filename, 'r') as csvfile: 
    # creating a csv reader object 
    csvreader = csv.reader(csvfile) 
      
    # extracting field names through first row 
    fields = csvreader.__next__() 
  
    # extracting each data row one by one 
    for row in csvreader: 
        rows.append(row) 
  
    # get total number of rows 
    print("Total no. of rows: %d"%(csvreader.line_num)) 
  
# printing the field names 
print('Field names are:' + ', '.join(field for field in fields)) 
  
#  printing first 5 rows 
print('\nFirst 5 rows are:\n') 
#file_name = []
#xmin =[]
#ymin = []
#xmax = []
#ymax = []
index3 = 1
index2 = 1
index = 1
jumlah = 0
data = []
file_name_prev= 'frame1.0.jpg'
file = 'frame1.0'
lastData = ''
jumlahGlobal = 0
width = 1
height = 1
label =''
xmin,ymin,xmax,ymax = 0,0,0,0
file_name_txt = file + '.txt'
# a = open(file_name_txt,'w+')
open_st = False

newFileName = 'train_labels4_truckv3.csv'
with open(newFileName, 'w', newline='') as csvfile:
    filewriter = csv.writer(csvfile, delimiter=',',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)

    baris = 0
    for row in rows: 
        ind = 0;
    # parsing each column of a row 
        for col in row: 
            if (ind==0):
                file_name = col
                (file, ext) = os.path.splitext(file_name)
                # print(file,' ', ext)
                file_name_txt = file + '.txt'
            if (ind==1):
                width = int(col) 
                width = int(width)
                if (width == 640):
                    width = width*0.4
                    width = int (width)
                    state = 1
                    print(state)
                    print(file,' ', ext)
                else:
                    state = 0
            if (ind==2):
                height = int(col)
                height = int(height)
                if (height == 480):
                    height = height*0.4
                    height = int(height)
            if (ind == 3):
                label = col
                # if label == 'car':
                #     obj = '0'
                # if label == 'motorbike':
                #     obj = '1'
                # if label == 'truck':
                #     obj = '2'
            if (ind==4):
                xmin = int(col)
                xmin = int(xmin)
                if (state == 1):
                    xmin = xmin*0.4
                    xmin = int(xmin)
            if (ind==5):
                ymin = int(col)
                ymin = int(ymin)
                if (state == 1):
                    ymin = ymin*0.4
                    ymin = int(ymin)
            if (ind==6):
                xmax = int(col)
                xmax = int(xmax)
                if (state == 1):
                    xmax = xmax*0.4
                    xmax = int(xmax)
            if (ind==7):
                ymax = int(col)
                ymax = int(ymax)
                if (state == 1):
                    ymax = ymax*0.4
                    ymax = int(ymax)
                
            ind = ind+1
        # print(filename, ' ',str(width),' ',str(height),' ',label,' ',\
        #           ' ',str(xmin),' ',str(ymin),' ',str(xmax),' ',str(ymax) )
            # toWrite = obj + ' ' + str(((xmax-xmin)/(2*width))) + ' '+ \
            # str(((ymax-ymin)/(2*height))) + ' '+ str(((xmax-xmin)/(width))) + \
            # ' '+ str(((ymax-ymin)/(height))) + '\n'
            
        filewriter.writerow([file_name,str(width),str(height),label,str(xmin),str(ymin)\
                                      ,str(xmax),str(ymax)])
    
# a.close()
    
    # if (file_name == 'frame1.0.jpg'):
    #     a.write(toWrite)
    # else:
    #     a.close()
    #     if(file_name_prev != file_name):
    #         if(open_st == False):
    #             f = open(file_name_txt,'w+')
    #             open_st = True
    #         else:
    #             open_st = False
    #             f.close()
    #             f = open(file_name_txt,'w+')
    #     elif(file_name_prev == file_name):
    #         f.write(toWrite)
    
    
    # file_name_prev = file_name
    
#    baris += 1
#    if (baris > 0):
#        print('lokasi ', rows[baris-1][0])    
    
    # Path = 'C:/Users/Aljovan Magyarsa P/Google Drive/Documents/Data Kuliah/Kerja Praktek/vehicle edit/Git Raspi/vehicle-classification/images/raw/'
    # raw = cv2.imread(Path+file_name)
    # # raw = cv2.imread(os.path.join(Path,file_name))
    # cv2.imshow('cro', raw)
    # scale_percent = 40 # percent of original size
    # width = int(raw.shape[1] * scale_percent / 100)
    # height = int(raw.shape[0] * scale_percent / 100)
    # dim = (width, height)
    # im = cv2.resize(raw, dim, interpolation = cv2.INTER_AREA)
    # cropped = im[ymin:ymax, xmin:xmax]
    # cv2.imshow('crop', cropped)
    # if cv2.waitKey(1) == ord('q'):
    #     break
    
    
    
    
    
# resize image
#    resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
#    
#    if (label == 'motorbike'):
#        if (namaFileSebelum == file_name):
#            jumlah = jumlah + 1
#            data.append('\t' + str(xmin)+ ' ' + str(ymin) +' '+ \
#                    str(xmax-xmin) + ' ' + str(ymax-ymin))
#        if (namaFileSebelum != file_name):
#            for datas in data:
#                lastData = lastData + datas
#            f.write("/home/jovan/Dataset/" + namaFileSebelum + '\t' + str(jumlah) + lastData + '\n')
#            jumlahGlobal = jumlah + jumlahGlobal
#            jumlah = 0
#            data = []
#            lastData = ''
#            namaFileSebelum = file_name
    

    
    
    
    
#    namaFile = label
#    if (label=='car'):
#        cv2.imwrite(namaFile+str(index)+'.jpg', cropped)
#        index = index + 1
#    if (label=='motorbike'):
#        cv2.imwrite(namaFile+str(index2)+ '.jpg', cropped)
#        index2 = index2 + 1
#    if (label=='truck'):
#        cv2.imwrite(namaFile+str(index3)+'.jpg', cropped)
#        index3 = index3 + 1
        
#    time.sleep(5)
    
    

#        print('col:', col, ' ind:', ind)
#        print('row:', row)
#        print("%10s"%col), 
#    print('\n')
        
print('file_name: ', file_name)

