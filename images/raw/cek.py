# -*- coding: utf-8 -*-
"""
Created on Fri Jan 31 09:56:48 2020

@author: Aljovan Magyarsa P
"""

import glob
import cv2
name = []

images=glob.glob("*.jpg")
for image in images:
    # print(image)
    raw = cv2.imread(image)
    scale_percent = 40 # percent of original size
    width = int(raw.shape[1] * scale_percent / 100)
    height = int(raw.shape[0] * scale_percent / 100)
    dim = (width, height)
    images= cv2.resize(raw, dim, interpolation = cv2.INTER_AREA)

    name = image
    cv2.imwrite(name,images)