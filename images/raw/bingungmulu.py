# -*- coding: utf-8 -*-
"""
Created on Mon Feb  3 14:57:23 2020

@author: Aljovan Magyarsa P
"""

import cv2

raw = cv2.imread('frame1.0.jpg')
scale_percent = 40 # percent of original size
width = int(raw.shape[1] * scale_percent / 100)
height = int(raw.shape[0] * scale_percent / 100)
dim = (width, height)
image= cv2.resize(raw, dim, interpolation = cv2.INTER_AREA)
    # cropped = im[ymin:ymax, xmin:xmax]
xmin = input('xmin:')
ymin = input('ymin:')
xmax = input('xmax:')
ymax = input('ymax:')

xmin,ymin,xmax,ymax = int(xmin),int(ymin), int(xmax),int(ymax)


cropped = image[ymin:ymax, xmin:xmax]
cv2.imshow("cropped", cropped)
cv2.waitKey(0)
# if cv2.waitKey(1) == ord('q'):
